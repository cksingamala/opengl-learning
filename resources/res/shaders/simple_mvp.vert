#version 330 

layout (location = 0) in vec3 vertexPosition;

uniform mat4 mvp;
uniform vec3 color;

out vec3 vColor;
void main()
{
	gl_Position = vec4(vertexPosition, 1.0);

	vColor = color + vec3(1.0,0.0,0.0);
}