#pragma once


#define GL3_PROTOTYPES 1
#include <GL/glew.h>


enum ATTRIBUT_LOCATIONS
{
	LOC_POSITION = 0,
	LOC_COLOR = 1,
	LOC_NORMAL = 2,
	LOC_UV0 = 3,
	LOC_UV1 = 4,
	LOC_UV2 = 5,
	LOC_UV3 = 6,
};