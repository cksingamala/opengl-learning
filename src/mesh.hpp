#pragma once
#include "opengl_headers.hpp"
#include <vector>
using namespace std;

class Mesh {
	//will always use index buffer
	//will create only one vbo


public:
	struct VertexDataLayout {
		int location;
		int start;
		int size;
		int offset;
	};
	Mesh() {}
	Mesh(vector<float> _vertexData, vector<unsigned short> _indices, vector<VertexDataLayout> _layoutList)
	{
		Load(_vertexData, _indices, _layoutList);
	}
	virtual ~Mesh() {
		Unload();
	}

	void Render() {
		glBindVertexArray(voaId);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}

protected:
	unsigned int voaId = 0;
	unsigned int vertexCount = 0;
	unsigned int vboId = 0;
	unsigned int iboId = 0;

	void Load(std::vector<float> _vertexData, std::vector<unsigned short> _indices, std::vector<VertexDataLayout> _layoutList)
	{
		vertexCount = (unsigned int)_indices.size();
		//create VAO
		glGenVertexArrays(1, &voaId);
		glBindVertexArray(voaId);
		//single vbo with mutliple attributes
		glGenBuffers(1, &vboId);
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBufferData(GL_ARRAY_BUFFER, _vertexData.size() * sizeof(float), _vertexData.data(), GL_STATIC_DRAW);

		for (int i = 0; i < _layoutList.size(); i++) {
			VertexDataLayout layout = _layoutList[i];
			glEnableVertexAttribArray(layout.location);
			glVertexAttribPointer(layout.location, layout.size, GL_FLOAT, GL_FALSE, layout.offset * sizeof(float), (void *)(layout.start * sizeof(float)));

		}

		glGenBuffers(1, &iboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(unsigned short), _indices.data(), GL_STATIC_DRAW);
	}

	void Unload()
	{
		glBindVertexArray(0);
		glDeleteVertexArrays(1, &voaId);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteBuffers(1, &vboId);
		glDeleteBuffers(1, &iboId);


	}
};

class TriangleMesh : public Mesh {

public:
	TriangleMesh()
	{
		vector<VertexDataLayout> _layoutList;

		GLfloat z_value = 1.0f;
		std::vector<GLfloat> vertexArray =
		{
			+0.0f, +1.0f, z_value,	 0.0f,  0.0f, -1.0f,	1.0f,  0.0f, 0.0f,						0.0f, 0.0f,
			+1.0f, -1.0f, z_value,	 0.0f,  0.0f, -1.0f, 	0.0f,  0.0f, 1.0f,						1.0f, 0.0f,
			-1.0f, -1.0f, z_value,	 0.0f,  0.0f, -1.0f, 	0.0f,  1.0f, 0.0f,						1.0f, 1.0f,
		};

		std::vector<GLushort> indices = {0,1,2};


		Load(vertexArray, indices, 
			{
				{ LOC_POSITION, 0, 3, 11 },
				{ LOC_COLOR, 6, 3, 11 }
			});
	}
	virtual ~TriangleMesh() {}
};