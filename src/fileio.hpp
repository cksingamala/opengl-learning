#pragma once
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class FileIO {
public:
	static vector<char> ReadBinaryFile(string filename)
	{
		ifstream ifs(filename, ios::binary | ios::ate);
		if (!ifs.good())
			return vector<char>();

		ifstream::pos_type pos = ifs.tellg();

		vector<char>  result(pos);

		ifs.seekg(0, ios::beg);
		ifs.read(&result[0], pos);

		result.push_back('\0');

		return result;
	}
};