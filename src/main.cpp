
#include <iostream>
#include <memory>
#include "defines.hpp"
#include "mesh.hpp"
#include "shader.hpp"
#include "glm/glm.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_transform.hpp"

using namespace std;
using namespace glm;

int main(int argc, char *argv[])
{
	string resourceDir = "";
	if (argc > 1)
		resourceDir = argv[1];
	{
		shared_ptr<DisplayManager> displayManager = make_shared<DisplayManager>();

		bool running = true;

		unique_ptr<TriangleMesh> triangleMesh = make_unique<TriangleMesh>();
		unique_ptr<Shader> shader = make_unique<Shader>(resourceDir + "res/shaders/simple_mvp.vert", resourceDir + "res/shaders/simple.frag");
		shader->Set();
		float test[3] = { 0.0f, 1.0f, 0.0f };
		shader->SetUniform("color", test);
		shader->Reset();

		while (running)
		{
			if (!displayManager->BeginFrame()) running = false;

			shader->Set();

			static int rot = 0;
			rot += 1;
			rot %= 360;
			mat4 mvp = mat4(1);
			mvp = rotate(mvp, (float)rot, vec3(0.0, 1.0, 0.0));

			shader->SetUniform("mvp", value_ptr(mvp));

			triangleMesh->Render();

			shader->Reset();

			if (!displayManager->EndFrame()) running = false;
		}
	}
	//cout << "press enter key to exit..." << endl;
	//cin.get();
    return 0;
}

