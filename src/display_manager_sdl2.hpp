#pragma once

#ifdef DISPLAY_MANAGER_SDL2

#include <iostream>
#include <string>

using namespace std;

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

#include "opengl_headers.hpp"


#include "display_config.hpp"
#include "log.hpp"

#define FPS_CHECK_EVERY		1	//1Secs


class DisplayManagerSDL2
{
	SDL_Window *window;
	SDL_GLContext glContext;

	Uint32 fpsLastTimeInMilliseconds;
	Uint32 frameCounter;

	Uint32 lastFrameTime = 0;
	Uint32 deltaTime = 0;


	void CheckSDLError(int line = -1)
	{
		std::string error = SDL_GetError();

		if (error != "")
		{
			cout << "SLD Error : " << error << std::endl;

			if (line != -1)
				cout << "\nLine : " << line << std::endl;

			SDL_ClearError();
		}
	}

	bool SetOpenGLAttributes()
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);


		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		fpsLastTimeInMilliseconds = SDL_GetTicks();
		frameCounter = 0;

		return true;
	}

	void Cleanup()
	{
		SDL_GL_DeleteContext(glContext);

		SDL_DestroyWindow(window);

		SDL_Quit();
	}

	void PrintSDL_GL_Attributes()
	{
		int value = 0;
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &value);
		cout << "SDL_GL_CONTEXT_MAJOR_VERSION : " << value << endl;

		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &value);
		cout << "SDL_GL_CONTEXT_MINOR_VERSION: " << value << endl;

		Log("GL Version %s", (const char *)glGetString(GL_VERSION));
		Log("GL Vendor %s", (const char *)glGetString(GL_VENDOR));
		Log("GL Renderer %s", (const char *)glGetString(GL_RENDERER));
		Log("GLSL version %s", (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION));
		//Log("GL Extensions %s", (const char *)glGetString(GL_EXTENSIONS));

	}


	void Init()
	{
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			std::cout << "Failed to init SDL\n";
			return ;
		}

		window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL);

		// Check that everything worked out okay
		if (!window)
		{
			std::cout << "Unable to create window\n";
			CheckSDLError(__LINE__);
			return ;
		}

		SetOpenGLAttributes();
		
		// Create our opengl context and attach it to our window
		glContext = SDL_GL_CreateContext(window);


		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			/* Problem: glewInit failed, something is seriously wrong. */
			fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		}
		fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	//	SDL_GL_SetSwapInterval(1);

		PrintSDL_GL_Attributes();

		fpsLastTimeInMilliseconds = SDL_GetTicks();


	}

	bool CheckForEvents()
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				return false;

			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					return false;
					break;
				case SDLK_r:
					// Cover with red and update
					glClearColor(1.0, 0.0, 0.0, 1.0);
					break;
				case SDLK_g:
					// Cover with green and update
					glClearColor(0.0, 1.0, 0.0, 1.0);
					break;
				case SDLK_b:
					// Cover with blue and update
					glClearColor(0.0, 0.0, 1.0, 1.0);

					break;
				default:
					break;
				}
			}
		}

		return true;
	}
public:
	DisplayManagerSDL2() {
		cout << "Display Manager with SDL2 Created" << endl;
		Init();
	}

	virtual ~DisplayManagerSDL2() {
		Cleanup();
		cout << "Display Manager Destroyed" << endl;
	}

	Uint32 GetDeltaTime() {
		return deltaTime;
	}

	bool BeginFrame() {

		deltaTime = SDL_GetTicks() - lastFrameTime;
		lastFrameTime = SDL_GetTicks();

		if (!CheckForEvents())
			return false;


		return true;
	}

	bool EndFrame() {

		SDL_GL_SwapWindow(window);
		frameCounter++;
		Uint32 diffTime = SDL_GetTicks() - fpsLastTimeInMilliseconds;
		if (diffTime  > FPS_CHECK_EVERY * 1000)
		{
			int fps =  frameCounter / FPS_CHECK_EVERY;
			frameCounter = 0;
			fpsLastTimeInMilliseconds = SDL_GetTicks();
			//cout << "FPS = " << fps << endl;
			char outStr[256] = "";
			sprintf_s(outStr, "%s %d", WINDOW_TITLE, fps);
			SDL_SetWindowTitle(window, outStr);
		}

		return true;
	}

};

typedef DisplayManagerSDL2 DisplayManager;



#endif

