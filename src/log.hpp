#pragma once

#include <assert.h>

#ifdef DISABLE_LOG
#define	 Log(...)	
#define  LogI(...)	
#define  LogD(...)  
#define  LogE(...)  
#else
#define	 Log(...)	{printf(__VA_ARGS__); printf("\n");}
#define  LogI(...)	{printf("Info:");  printf(__VA_ARGS__); printf("\n");}
#define  LogD(...)  {printf("Debug:"); printf(__VA_ARGS__); printf("\n");}
#define  LogE(...)  {printf("Error:"); printf(__VA_ARGS__); printf("\n"); assert(0);}
#endif